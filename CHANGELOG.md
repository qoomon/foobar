<a name=""></a>
# [](https://gitlab.com/qoomon/foobar/compare/v2.0.0...v) (2016-05-20)



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/qoomon/foobar/compare/v1.0.0...v2.0.0) (2016-05-20)


### Features

* **Foo:** new new new ([df9a42a](https://gitlab.com/qoomon/foobar/commit/df9a42a))
* **Foo:** new new new ([2b61380](https://gitlab.com/qoomon/foobar/commit/2b61380))
* **Foo:** new new new ([4a4111f](https://gitlab.com/qoomon/foobar/commit/4a4111f))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/qoomon/foobar/compare/acd7115...v1.0.0) (2016-05-13)


### Features

* test ([acd7115](https://gitlab.com/qoomon/foobar/commit/acd7115))



